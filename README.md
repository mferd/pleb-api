# pleb-api

API for `pleb-app`

## Installing

Make sure NodeJS and npm are installed first

## Dependency installation

Requires a MongoDB (database) and Redis (sessions) server

```
$ npm install
```

## Configuration

The `Discord Client ID` and `Discord Client Secret` must be set as environment variables (Added to .bash_profile, .zshenv or equivalent)

## Running

```
$ npm start
```
