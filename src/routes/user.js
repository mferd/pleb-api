const User = require('../models/user');

module.exports = {
  // Returns all users
  list: async (ctx) => {
    console.log('here');
    if (ctx.isAuthenticated()) {
      const user = await User.find({}).sort({ username: 'asc' });
      if (user) {
        ctx.body = user;
      } else {
        ctx.status = 401;
      }
    } else { ctx.status = 401; }
  },
  // Returns a specific user by username
  one: async (ctx, username) => {
    if (ctx.isAuthenticated()) {
      const user = await User.find({
        username,
      });
      if (user) {
        ctx.body = user;
      } else {
        ctx.status = 401;
      }
    } else { ctx.status = 401; }
  },
  addXp: async (ctx, userId) => {
    if (ctx.isAuthenticated()) {
      const user = await User.find({
        _id: userId,
      });
      if (user) {
        const xp = ctx.request.body.xp;
        ctx.body = await User.giveXp(ctx.state.user, xp);
      }
    } else { ctx.status = 401; }
  },
};
