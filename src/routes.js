const passport = require('koa-passport');
const userRoutes = require('./routes/user');
const soundRoutes = require('./routes/sound');
const _ = require('koa-route');
const jwt = require('jsonwebtoken');
const config = require('./config');

function getPrefix() {
  return '/api';
}

function callbackFn(ctx, next) {
  return new Promise((resolve, reject) =>
    passport.authenticate('discord', {
      successRedirect: `${config.localServer || ''}/`,
      failureRedirect: '/error',
      failureFlash: true,
      session: false,
    }, (err, user) => {
      console.log(`${config.localServer || ''}/?token=${jwt.sign(
        { id: user.user._id }, config.secrets[0])}`);
      resolve(ctx.redirect(`${config.localServer || ''}/#/?token=${jwt.sign({
        id: user.user._id,
      }, config.secrets[0])}`));
    })(ctx, next).catch(err => reject(err)));
}

module.exports.public = [
  _.get(`${getPrefix()}/auth/discord`, passport.authenticate('discord')),
  _.get(`${getPrefix()}/callback`, callbackFn),
];

module.exports.auth = [
  _.get(`${getPrefix()}/sound`, soundRoutes.list),
  _.get(`${getPrefix()}/sound/:sound`, soundRoutes.one),
  _.post(`${getPrefix()}/sound/:sound/upvote`, soundRoutes.upvote),
  _.post(`${getPrefix()}/sound/:sound/downvote`, soundRoutes.downvote),
  _.post(`${getPrefix()}/sound/:sound/undovote`, soundRoutes.removeVote),

  _.get(`${getPrefix()}/user`, userRoutes.list),
  _.get(`${getPrefix()}/user/:user`, userRoutes.one),
  _.post(`${getPrefix()}/user/:user/xp`, userRoutes.addXp),
];
