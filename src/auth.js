const passport = require('koa-passport');
const DiscordStrategy = require('passport-discord').Strategy;
const config = require('./config');
const User = require('./models/user');

const scopes = ['identify'];

// Authenticate with Discord
passport.use(new DiscordStrategy({
  clientID: config.clientId,
  clientSecret: config.clientSecret,
  callbackURL: `${config.callbackUri || ''}/api/callback`,
  scope: scopes,
},
(accessToken, refreshToken, profile, done) => {
  console.log(accessToken);
  User.findOrCreate(profile)
    .then(user => done(null, { user, accessToken, refreshToken }))
    .catch(err => done(err));
},
));
passport.serializeUser((user, done) => {
  console.log('here');
  done(null, user._id);
});
passport.deserializeUser((id, done) => {
  User.findById(id, done);
});
