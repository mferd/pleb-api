const mongoose = require('mongoose');
const AppError = require('../AppError');

mongoose.Promise = global.Promise;
const Schema = mongoose.Schema;
const VoteSchema = new Schema({
  user: {
    type: String,
    ref: 'User',
  },
  sound: {
    type: Schema.Types.ObjectId,
    ref: 'Sound',
  },
  rating: Number,
});

class VoteClass {
  static async rate(user, sound, rating) {
    let diff = rating;
    let vote = await sound.votes.find(v => v.user === user._id);
    if (vote) {
      if (vote.rating === rating) {
        throw new AppError('Vote already exists', 400);
      }
      diff = rating - vote.rating;
      vote.rating = rating;
    } else {
      vote = new this({
        user: user._id,
        sound: sound._id,
        rating,
      });
      console.log(user);
      user.votes.push(vote);
      sound.votes.push(vote);
    }
    await vote.save();
    await user.save();
    sound.points += diff;
    return sound.save();
  }
}
VoteSchema.loadClass(VoteClass);

module.exports = mongoose.model('Vote', VoteSchema);
